import { NavigationContainer, StackActions } from "@react-navigation/native";
import React, { Component, useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Button,
  TextInput,
  SafeAreaView,
} from "react-native";
import {
  createStackNavigator,
  HeaderBackButton,
} from "@react-navigation/stack";
import LoginScreen from "../src/screens/LoginScreen";
import SignupScreen from "../src/screens/SignupScreen";
import ForgotPasswordScreen from "../src/screens/ForgotPasswordScreen";
import HomeScreen from "../src/screens/HomeScreen";
import BookMainScreen from "../src/screens/BookMainScreen";

import ChapterScreen from "../src/screens/ChapterScreen";
import PlaylistScreen from "../src/screens/PlaylistScreen";
import AsyncStorage from "@react-native-community/async-storage";
import ProfileScreen from "../src/screens/ProfileScreen";
import store from "../redux/stores/store";
import ImageScreen from "../src/screens/ImageScreen";
import LikeImageScreen from "../src/screens/LikeImageScreen";
import MapScreen from "../src/screens/MapScreen";
import ContactScreen from "../src/screens/ContactScreen";
import ApiScreen from "../src/screens/ApiScreen";
import ChatScreen from "../src/screens/ChatScreen";

const Stack = createStackNavigator();

function SignIn() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="login"
        options={{ headerShown: false }}
        component={LoginScreen}
      />
      <Stack.Screen
        name="signup"
        options={{
          headerTitle: "Create Your Account",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            alignSelf: "flex-end",
            fontSize: 22,
            fontFamily: "Raleway-SemiBold",
          },

          headerStyle: {
            backgroundColor: "#FFFFFF",
            borderBottomWidth: 0,
          },
        }}
        component={SignupScreen}
      />
      <Stack.Screen
        name="forgotPassword"
        options={{
          headerTitle: null,
        }}
        component={ForgotPasswordScreen}
      />
    </Stack.Navigator>
  );
}
function Home() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="homescreen"
        options={{
          headerTitle: "MOSHE FISHMAN",
          headerTintColor: "royalblue",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
            textAlign: "left",
          },

          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={HomeScreen}
      />
      <Stack.Screen
        name="book"
        options={{
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={BookMainScreen}
      />
      <Stack.Screen
        name="chapter"
        options={{
          headerTitle: "ChapterScreen",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ChapterScreen}
      />

      <Stack.Screen
        name="music"
        options={{
          headerTitle: "MusicScreen",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={PlaylistScreen}
      />
      <Stack.Screen
        name="profile"
        options={{
          headerTitle: "Profile",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ProfileScreen}
      />
      <Stack.Screen
        name="mapScreen"
        options={{
          headerTitle: "User Location",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={MapScreen}
      />
      <Stack.Screen
        name="apiScreen"
        options={{
          headerTitle: "USER INFORMATION",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ApiScreen}
      />
      <Stack.Screen
        name="chatScreen"
        options={{
          headerTitle: "Chat",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ChatScreen}
      />
      <Stack.Screen
        name="contactScreen"
        options={{
          headerTitle: "Contacts",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ContactScreen}
      />
      <Stack.Screen
        name="image"
        options={{
          headerTitle: "Image",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={ImageScreen}
      />
      <Stack.Screen
        name="likeScreen"
        options={{
          headerTitle: "Like Images",
          headerTintColor: "white",
          headerBackTitleVisible: false,

          headerTitleStyle: {
            fontSize: 18,
            fontFamily: "Raleway-SemiBold",
            color: "#FFFFFF",
          },
          headerStyle: {
            backgroundColor: "#030504",
          },
        }}
        component={LikeImageScreen}
      />
    </Stack.Navigator>
  );
}

const RouteApp = ({ navigation }) => {
  const [authenticated, setAuthenticated] = useState("");
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    AsyncStorage.getItem("userData")
      .then((res) => {
        let userInfo = JSON.parse(res);
        store.dispatch({ type: "USER_DATA", payload: userInfo });
        setAuthenticated(userInfo);

        setLoading(true);
      })
      .catch((err) => {
        setLoading(true);
      });
  }, []);

  if (!isLoading) {
    return null;
  }

  return (
    <NavigationContainer>
      {authenticated ? (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="home" component={Home} />
          <Stack.Screen name="SignIn" component={SignIn} />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="home" component={Home} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

export default RouteApp;
