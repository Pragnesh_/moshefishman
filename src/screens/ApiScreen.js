import React, { useState } from "react";

import {
  SafeAreaView,
  View,
  Button,
  StyleSheet,
  Text,
  FlatList,
  ActivityIndicator,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { getUniversity } from "../global/function";

const ApiScreen = () => {
  const [getData, setGetData] = useState([]);
  const [loading, setLoading] = useState(true);

  React.useEffect(() => {
    getApiData();
  }, []);

  const getApiData = () => {
    getUniversity()
      .then((response) => {
        setLoading(false);
        setGetData(response.response_data.university_data);
      })
      .catch((error) => {
        setLoading(true);
        console.log("Response Error", error);
      });
  };

  const ItemView = ({ item }) => {
    return (
      <View style={styles.flatView}>
        <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
          {item.university_name}
        </Text>
        <Text style={styles.email}>{item.university_domain}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.h2text}>University Info</Text>
      {!loading ? (
        <FlatList
          data={getData}
          showsVerticalScrollIndicator={false}
          renderItem={ItemView}
          keyExtractor={(item) => item.email}
        />
      ) : (
        <View style={[styles.containers, styles.horizontal]}>
          <ActivityIndicator size="large" color="red" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "skyblue",
  },
  containers: {
    flex: 1,
    justifyContent: "center",
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
  h2text: {
    marginTop: 10,
    fontFamily: "Helvetica",
    fontSize: 36,
    fontWeight: "bold",
  },
  flatView: {
    paddingTop: 30,
    borderRadius: 2,
  },
  name: {
    fontFamily: "Verdana",
    fontSize: 18,
  },
  email: {
    color: "red",
  },
});

export default ApiScreen;
