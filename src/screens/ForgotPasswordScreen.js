import React, { Component, useState } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { validateEmail } from "../global/function";

const ForgotPasswordScreen = () => {
  const [emailError, setEmailError] = useState(false);
  const [email, setEmail] = useState("");

  const _onChangeEmail = (text) => {
    setEmail(text);
    if (validateEmail(text)) {
      setEmailError(false);
    } else {
      setEmailError(true);
    }
  };

  const _onLogin = () => {
    if (validateEmail(email)) {
      alert("Reset Password Successs");
    } else {
      alert("Please enter valid email id");
    }
  };
  

  return (
    <View style={styles.MainContainer}>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ alignItems: "center" }}
      >
        <View style={{ marginTop: hp("5%") }}>
          <Image
            style={{
              resizeMode: "contain",
              height: hp("15%"),
              width: 200,
              alignSelf: "center",
            }}
            source={require("../assets/images/vector_reset_password.png")}
          ></Image>
          <Text style={styles.forgotTitleText}>Forgot Password?</Text>
          <Text style={styles.resetTitleText}>Enter your email to</Text>
          <Text style={styles.resetTitleText}>reset account password</Text>
        </View>

        <View style={{ marginTop: hp("4%"), width: wp("80%") }}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_email_black.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text, email) => {
                _onChangeEmail(text);
                
              }}
            />
            {emailError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>
          <View style={{ marginTop: hp("3%") }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => {
                _onLogin();
              }}
            >
              <Text style={styles.loginText}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>

        <SafeAreaView />
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
  },
  loginScreenButton: {
    width: wp("80%"),
    backgroundColor: "#030504",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  loginText: {
    color: "#FFFFFF",
    padding: 10,
    fontSize: wp("5%"),
    fontFamily: "Raleway-SemiBold",
  },
  forgotTitleText: {
    color: "#111111",
    padding: 10,
    fontSize: wp("7%"),
    fontFamily: "Raleway-SemiBold",
  },
  resetTitleText: {
    color: "#9B9A9B",
    alignItems: "center",
    alignSelf: "center",
    fontSize: wp("5%"),
    fontFamily: "Raleway-Regular",
  },
  ImageStyle: {
    marginLeft: wp("3%"),
    alignItems: "center",
  },

  TextInputStyleClass: {
    flex: 1,
    fontSize: 17,
    color: "#030504",
    marginLeft: wp("3%"),
    height: 50,
    fontFamily: "Raleway-Regular",
  },

  SectionStyle: {
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9B9A9B",
  },
});

export default ForgotPasswordScreen;
