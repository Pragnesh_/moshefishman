import React, { Component, useEffect, useState } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  YellowBox,
  TextInput,
  Alert,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import SplashScreen from "react-native-splash-screen";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { CommonActions } from "@react-navigation/native";
import SignupScreen from "./SignupScreen";
import AsyncStorage from "@react-native-community/async-storage";
import store from "../../redux/stores/store";
import RNLocation from "react-native-location";
import crashlytics from "@react-native-firebase/crashlytics";
import Firebase from "@react-native-firebase/app";
import messaging from "@react-native-firebase/messaging";
var PushNotification = require("react-native-push-notification");

const LoginScreen = ({ navigation }) => {
  console.log("====================================");
  console.log(
    RNLocation.requestPermission({
      ios: "whenInUse",
      android: {
        detail: "coarse",
      },
    }).then((granted) => {
      if (granted) {
        locationSubscription = RNLocation.subscribeToLocationUpdates(
          (locations) => {
            console.log(locations);
          }
        );
      }
    })
  );
  console.log("====================================");

  const userinfo = { username: "Admin", password: "12345" };
  let userData = {
    userFname: "admin",
    userLname: "admin",
    userEmail: "admin@gmail.com",
    userDate: "01-01-2000",
  };
  useEffect(() => {
    crashlytics().log("User signed in.");
    SplashScreen.hide();
    Firebase.initializeApp();
    PushNotification.configure({
      onRegister: function (token) {
        console.log("TOKEN:", token);
      },

      onNotification: function (notification) {
        foreground: false;
        console.log("NOTIFICATION:", notification);

        console.log("hey", notification.title);
      },

      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);
      },

      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      PushNotification: {
        foreground: true,
      },

      popInitialNotification: true,

      requestPermissions: true,
    });
  }, []);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [unameError, setUnameError] = useState(false);
  const [passError, setPassError] = useState(false);

  const _onLogin = () => {
    if (userinfo.username === username && userinfo.password === password) {
      AsyncStorage.setItem("userData", JSON.stringify(userData));
      store.dispatch({ type: "USER_DATA", payload: userData });
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "home" }],
        })
      );
    } else {
      alert("Please enter valid Username and Password");
    }
  };

  const _onChangeUsername = (text) => {
    setUsername(text);

    if (text === "") {
      setUnameError(true);
    } else {
      setUnameError(false);
    }
  };

  const _onChangePassword = (text) => {
    setPassword(text);
    if (text === "") {
      setPassError(true);
    } else {
      setPassError(false);
    }
  };

  

  return (
    <View style={styles.MainContainer}>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ alignItems: "center" }}
      >
        <View style={{ marginTop: hp("15%") }}>
          <Image
            style={{ resizeMode: "contain", height: hp("20%"), width: 200 }}
            source={require("../assets/images/Logo_white.png")}
          ></Image>
        </View>

        <View style={{ marginTop: hp("10%"), width: wp("80%") }}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_email.png")}
              style={styles.ImageStyle}
            />

            <TextInput
              style={{
                flex: 1,
                fontSize: 17,
                color: "#FFFFFF",
                marginLeft: wp("2%"),
                fontFamily: "Raleway-Regular",
              }}
              onChangeText={(text) => _onChangeUsername(text)}
              placeholder="Username"
              underlineColorAndroid="transparent"
              placeholderTextColor="#FFFFFF"
              value={username}
            />
            {unameError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>

          <View
            style={{
              height: 1,
              marginVertical: 10,
              backgroundColor: "#9B9A9B",
              width: "100%",
            }}
          />

          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_password.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={{
                flex: 1,
                fontSize: 17,
                marginLeft: wp("2%"),
                fontFamily: "Raleway-Regular",
                color: "#FFFFFF",
              }}
              onChangeText={(text) => _onChangePassword(text)}
              secureTextEntry={true}
              placeholder="Password"
              underlineColorAndroid="transparent"
              placeholderTextColor="#FFFFFF"
              value={password}
            />
            {passError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>
          <View style={{ alignItems: "flex-end", marginTop: hp("2%") }}>
            <TouchableOpacity
              onPress={() => navigation.navigate("forgotPassword")}
            >
              <Text
                style={{
                  color: "#FFFFFF",
                  fontSize: 14,
                  fontFamily: "Raleway-Medium",
                }}
              >
                Forgot Password?
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: hp("8%") }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => {
                _onLogin();
              }}
            >
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "flex-end",
              justifyContent: "center",
              marginTop: hp("10%"),
            }}
          >
            <Text
              style={{
                color: "#FFFFFF",
                fontSize: 15,
                alignItems: "center",
                fontFamily: "Raleway-Regular",
              }}
            >
              Don't have an account?
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate("signup")}>
              <Text
                style={{
                  color: "#FFFFFF",
                  fontSize: 15,
                  marginLeft: 5,
                  fontFamily: "Raleway-Medium",
                }}
              >
                Create Now
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <SafeAreaView />
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#030504",
  },
  loginScreenButton: {
    width: wp("80%"),
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  loginText: {
    color: "#030504",
    padding: 15,
    fontSize: wp("5%"),
    fontFamily: "Raleway-SemiBold",
  },
  SectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // width: wp('70%'),
  },

  ImageStyle: {
    marginLeft: wp("3%"),
    alignItems: "center",
  },
});

export default LoginScreen;
