import React, { Component, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import store from "../../redux/stores/store";
import { connect, useSelector } from "react-redux";


import userHomeIcon from "../global/userHomeIcon";

const LikeImageScreen = (props) => {
  const { navigation, route } = props;
  const { itemImage } = useSelector((state) => ({
    itemImage: state.user.likeImage,
  }));
  navigation.setOptions({
    headerRight: () => userHomeIcon(navigation),
  });

  const _onLikeImage = (uri) => {
    store.dispatch({ type: "LIKE_IMAGE", payload: uri });
  };

  return (
    <View style={styles.container}>
      {!itemImage.length ? (
        <Text style={styles.likeTextStyle}>Your wishlist is empty</Text>
      ) : (
        <FlatList
          data={itemImage}
          renderItem={({ item }) => (
            <View
              style={{
                flex: 1,
                margin: 1,
              }}
            >
              <TouchableOpacity
                style={styles.likeImageStyle}
                onPress={() => _onLikeImage(item)}
              >
                <Image source={require("../assets/images/ic_like_full.png")} />
              </TouchableOpacity>

              <Image style={styles.imageThumbnail} source={{ uri: item }} />
            </View>
          )}
          numColumns={2}
          keyExtractor={(item, index) => index}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
  },
  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",
    height: hp("30%"),
    resizeMode: "cover",
  },
  likeTextStyle: {
    color: "#800000",
    alignSelf: "center",
    fontSize: wp("6%"),
    fontFamily: "Raleway-SemiBold",
  },
  likeImageStyle: {
    position: "absolute",
    right: wp("1%"),
    top: 2,

    zIndex: 1,
  },
});

export default LikeImageScreen;
