import React, { Component, useState } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  SafeAreaView,
  Image,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import ProgressImage from "react-native-image-progress";
import ProgressBar from "react-native-progress/Bar";

import store from "../../redux/stores/store";
import { connect, useSelector } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import userHomeListIcon from "../global/userHomeListIcon";

const mapStateToProps = (state) => {
  return {
    likeImage: state.user.likeImage,
  };
};


const ImageScreen = (props) => {
  const { navigation, route } = props;
  const [isLikeImage, setIsLikeImage] = useState(false);

  navigation.setOptions({
    headerRight: () => heartImage(),
  });

  const { itemImage } = useSelector((state) => ({
    itemImage: state.user.likeImage,
  }));

  const heartImage = () => {
    let index = itemImage.findIndex((obj) => obj === route.params.imageData);

    if (index !== -1) {
      setIsLikeImage(true);
    } else {
      setIsLikeImage(false);
    }

    return (
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity onPress={() => _onLikeImage()}>
          <Image
            source={
              !isLikeImage
                ? require("../assets/images/icon_like.png")
                : require("../assets/images/ic_like_full.png")
            }
            style={{ tintColor: "#FFF" }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            marginHorizontal: 20,
          }}
          onPress={() => navigation.navigate("homescreen")}
        >
          <Image source={require("../assets/images/ic_home.png")} />
        </TouchableOpacity>
      </View>
    );
  };

 
  const _onLikeImage = () => {
    // AsyncStorage.setItem("likeImage", JSON.stringify(isLikeImage));
    store.dispatch({ type: "LIKE_IMAGE", payload: route.params.imageData });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        {/* <Image style={styles.imageStyle} source={{uri: route.params.imageData}}  ></Image> */}
        <ProgressImage
          source={{ uri: route.params.imageData }}
          indicator={ProgressBar}
          style={{ width: "100%", height: "100%" }}
          resizeMode="contain"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba( 0, 0, 0, 0.6 )",
  },
  item: {
    fontSize: wp("3.5%"),
    fontFamily: "Raleway-SemiBold",
    color: "#9B9A9B",
  },
  imageStyle: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});

export default connect(mapStateToProps)(ImageScreen);
