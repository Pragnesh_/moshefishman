import React, { Component, useState, useEffect, useRef } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Alert,
} from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import DatePicker from "react-native-datepicker";
import { validateEmail } from "../global/function";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import store from "../../redux/stores/store";
import { connect } from "react-redux";
import userLocationIcon from "../global/userLocationIcon";

const mapStateToProps = (state) => {
  return {
    userData: state.user.userData,
  };
};

const ProfileScreen = (props) => {
  const { userData, navigation } = props;
  const [date, setDate] = useState(userData.userDate);
  const [firstNameError, setFirstNameError] = useState(false);
  const [firstName, setFirstName] = useState(userData.userFname);
  const [lastNameError, setLastNameError] = useState(false);
  const [lastName, setLastName] = useState(userData.userLname);
  const [emailError, setEmailError] = useState(false);
  const [email, setEmail] = useState(userData.userEmail);
  const [datePickerRef, setDatePickerRef] = useState("");
  const [isOldData, setIsOldData] = useState(true);
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var da = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year

    setCurrentDate(da + "-" + month + "-" + year);
  }, []);

  navigation.setOptions({
    headerRight: () => userLocationIcon(navigation),
  });
  const _onChangeFirstName = (text) => {
    setFirstName(text.trim());
    if (text === "") {
      setFirstNameError(true);
    } else {
      setFirstNameError(false);
    }
    CheckOldData("userFname", text);
  };

  const CheckOldData = (type, text) => {
    if (userData[type] === text) {
      setIsOldData(true);
    } else {
      setIsOldData(false);
    }
  };

  const _onChangeLname = (text) => {
    setLastName(text.trim());
    if (text === "") {
      setLastNameError(true);
    } else {
      setLastNameError(false);
    }
    CheckOldData("userLname", text);
  };

  const _onChangeEmail = (text) => {
    setEmail(text.trim());
    validateEmail(text)
      .then((text) => {
        setEmailError(false);
      })
      .catch((error) => {
        setEmailError(true);
      });
    CheckOldData("userEmail", text);
  };

  const _onUpdateProfile = () => {
    let userUpdated = {
      userFname: firstName,
      userLname: lastName,
      userEmail: email,
      userDate: date,
    };

    if (firstName && lastName) {
      if (date) {
        if (email && !emailError) {
          setIsOldData(true);
          AsyncStorage.setItem("userData", JSON.stringify(userUpdated));
          store.dispatch({ type: "USER_DATA", payload: userUpdated });
          alert("Update successfully");
        } else {
          alert("You have entered an invalid e-mail address. Please try again");
        }
      } else {
        alert("Please Enter Valid Date");
      }
    } else {
      alert("Please fill up above information");
    }
  };

  const _onLogout = () => {
    Alert.alert("Confirm", "Are you sure that you want to logout?", [
      {
        text: "Yes",
        onPress: () => {
          store.dispatch({ type: "USER_DATA", payload: "" });
          AsyncStorage.removeItem("userData");
          //navigation.navigate("SignIn");
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "SignIn" }],
            })
          );
        },
      },
      { text: "Cancel" },
    ]);
  };

  return (
    <View style={styles.MainContainer}>
      <DatePicker
        style={{
          position: "absolute",
        }}
        date={date}
        mode="date"
        showIcon={false}
        hideText={true}
        format="DD-MM-YYYY"
        confirmBtnText="Confirm"
        maxDate={currentDate}
        cancelBtnText="Cancel"
        ref={(dateReference) => {
          setDatePickerRef(dateReference);
        }}
        onDateChange={(res) => {
          setDate(res);
          CheckOldData("userDate", res);
        }}
      />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ alignItems: "center" }}
      >
        <View style={{ marginTop: hp("4%"), width: wp("80%") }}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_user_name.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="First Name"
              value={firstName}
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangeFirstName(text);
              }}
            />

            {firstNameError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>

          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_user_name.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="Last Name"
              value={lastName}
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangeLname(text);
              }}
            />
            {lastNameError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_email_black.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="Email"
              keyboardType="email-address"
              value={email}
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangeEmail(text);
              }}
            />
            {emailError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>

          <TouchableOpacity
            style={styles.SectionStyle}
            onPress={() => {
              datePickerRef.onPressDate();
            }}
          >
            <Image
              source={require("../assets/images/ic_dob.png")}
              style={styles.ImageStyle}
            />
            <Text style={styles.TextInputStyleClass}>{date}</Text>
          </TouchableOpacity>

          <View style={{ marginTop: hp("3%") }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => {
                if (isOldData) {
                  _onLogout();
                } else {
                  _onUpdateProfile();
                }
              }}
            >
              <Text style={styles.loginText}>
                {isOldData ? "Logout" : "Update"}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <SafeAreaView />
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
  },
  loginScreenButton: {
    width: wp("80%"),

    backgroundColor: "#030504",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  loginText: {
    color: "#FFFFFF",
    padding: 10,
    fontSize: wp("5%"),
    fontFamily: "Raleway-SemiBold",
  },

  ImageStyle: {
    marginLeft: wp("3%"),
    alignItems: "center",
  },

  TextInputStyleClass: {
    flex: 1,
    fontSize: 17,
    color: "#030504",
    marginLeft: wp("3%"),
    fontFamily: "Raleway-Regular",
    paddingVertical: hp("1%"),
  },
  TextInputStyle: {
    flex: 1,
    fontSize: 17,
    color: "#9B9A9B",
    marginLeft: wp("3%"),
    fontFamily: "Raleway-Regular",
    paddingVertical: hp("1%"),
  },

  RadioMainView: {
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "space-evenly",
    marginTop: 5,
  },
  RadioTouchable: { flexDirection: "row", alignItems: "center" },
  RadioTextSelect: {
    marginLeft: 4,
    fontSize: 16,
    fontFamily: "Raleway-Regular",
    color: "#030504",
  },
  RadioTextUnselect: {
    marginLeft: 4,
    fontSize: 16,
    fontFamily: "Raleway-Regular",
    color: "#A5A5A5",
  },
  SectionStyle: {
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9B9A9B",
  },
  PasswordSectionStyle: {
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9B9A9B",
    marginTop: 20,
  },
  PasswordErrorStyle: { height: 15, width: 15, marginRight: 10 },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
});

export default connect(mapStateToProps)(ProfileScreen);
