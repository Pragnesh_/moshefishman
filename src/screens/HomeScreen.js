import React, { Component, useState } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Alert,
} from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import SplashScreen from "react-native-splash-screen";
import likeListIcon from "../global/likeListIcon";
import userHomeListIcon from "../global/userHomeListIcon";
import ImagePicker from "react-native-image-picker";
import ActionButton from "react-native-action-button";
import DocumentPicker from "react-native-document-picker";
import crashlytics from "@react-native-firebase/crashlytics";

const listArray = [
  {
    value: "BAYTZA",
    type: "folder",
    subData: [
      { value: "book 11", type: "book" },
      {
        value: "book 12",
        type: "folder",
        subChapterData: [
          {
            value: "book 111",
            type: "folder",
            subChapterData: [
              {
                value: "book 1111",
                type: "book",
                uri: "http://www.africau.edu/images/default/sample.pdf",
              },
            ],
          },
        ],
      },
      {
        id: "13",
        value: "Audio 13",
        type: "audio",
        musicAdd: {
          id: "local-track",
          url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3",
          title: "Music 1",
          artist: "David Chavez",
          artwork: "https://i.picsum.photos/id/500/200/200.jpg",
        },
      },
    ],
  },
  {
    value: "CHALLA",
    type: "folder",
    subData: [
      {
        value: "book 21",
        type: "book",
        uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
      },
      { value: "book 22", type: "folder" },
      { value: "book 23", type: "audio" },
      {
        value: "Image 24",
        type: "image",
        imageUri: "https://homepages.cae.wisc.edu/~ece533/images/fruits.png",
      },
    ],
  },
  {
    value: "CHAGIGA",
    type: "book",
    uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
  },
  {
    value: "BAKURIM",
    type: "image",
    imageUri:
      "content://com.moshefishman.provider/root/storage/emulated/0/Pictures/images/image-165a163d-8586-4f27-822f-3b0edb044261.jpg",
  },
  {
    value: "NID",
    type: "folder",
    subData: [
      {
        value: "book 31",
        type: "book",
        uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
      },
      {
        value: "book 32",
        type: "folder",
        subChapterData: [
          {
            value: "book 111",
            type: "book",
            uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
          },
          {
            value: "book 111",
            type: "audio",
          },
        ],
      },
      { value: "book 33", type: "audio" },
    ],
  },
  {
    value: "BAYT",
    type: "audio",

    musicAdd: {
      id: "local",
      url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
      title: "Music 2",
      artist: "David",
      artwork: "https://i.picsum.photos/id/500/200/200.jpg",
    },
  },
  {
    value: "BAYTZA",
    type: "folder",
    subData: [
      { value: "book 11", type: "book" },
      {
        value: "book 12",
        type: "folder",
        subChapterData: [
          {
            value: "book 111",
            type: "folder",
            subChapterData: [
              {
                value: "book 1111",
                type: "book",
                uri: "http://www.africau.edu/images/default/sample.pdf",
              },
            ],
          },
        ],
      },
      {
        id: "13",
        value: "Audio 13",
        type: "audio",
        musicAdd: {
          id: "local-track",
          url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3",
          title: "Music 1",
          artist: "David Chavez",
          artwork: "https://i.picsum.photos/id/500/200/200.jpg",
        },
      },
    ],
  },
  {
    value: "CHALLA",
    type: "folder",
    subData: [
      {
        value: "book 21",
        type: "book",
        uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
      },
      { value: "book 22", type: "folder" },
      { value: "book 23", type: "audio" },
      {
        value: "Image 24",
        type: "image",
        imageUri:
          "https://4.bp.blogspot.com/-lYq2CzKT12k/VVR_atacIWI/AAAAAAABiwk/ZDXJa9dhUh8/s0/Convict_Lake_Autumn_View_uhd.jpg",
      },
    ],
  },
  {
    value: "CHAGIGA",
    type: "book",
    uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
  },
  {
    value: "BAKURIM",
    type: "image",
    imageUri: "https://homepages.cae.wisc.edu/~ece533/images/cat.png",
  },
  {
    value: "NID",
    type: "folder",
    subData: [
      {
        value: "book 31",
        type: "book",
        uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
      },
      {
        value: "book 32",
        type: "folder",
        subChapterData: [
          {
            value: "book 111",
            type: "book",
            uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
          },
          {
            value: "book 111",
            type: "audio",
          },
        ],
      },
      { value: "book 33", type: "audio" },
    ],
  },
  {
    value: "BAYT",
    type: "audio",

    musicAdd: {
      id: "local",
      url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
      title: "Music 2",
      artist: "David",
      artwork: "https://i.picsum.photos/id/500/200/200.jpg",
    },
  },
  {
    value: "BAYTZA",
    type: "folder",
    subData: [
      { value: "book 11", type: "book" },
      {
        value: "book 12",
        type: "folder",
        subChapterData: [
          {
            value: "book 111",
            type: "folder",
            subChapterData: [
              {
                value: "book 1111",
                type: "book",
                uri: "http://www.africau.edu/images/default/sample.pdf",
              },
            ],
          },
        ],
      },
      {
        value: "Audio 13",
        type: "audio",
        musicAdd: {
          id: "local-track",
          url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3",
          title: "Music 1",
          artist: "David Chavez",
          artwork: "https://i.picsum.photos/id/500/200/200.jpg",
        },
      },
    ],
  },
  {
    value: "CHALLA",
    type: "folder",
    subData: [
      {
        value: "book 21",
        type: "book",
        uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
      },
      { value: "book 22", type: "folder" },
      { value: "book 23", type: "audio" },
      {
        value: "Image 24",
        type: "image",
        imageUri:
          "https://4.bp.blogspot.com/-lYq2CzKT12k/VVR_atacIWI/AAAAAAABiwk/ZDXJa9dhUh8/s0/Convict_Lake_Autumn_View_uhd.jpg",
      },
    ],
  },
  {
    value: "BAYT",
    type: "audio",

    musicAdd: {
      id: "local",
      url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
      title: "Music 2",
      artist: "David",
      artwork: "https://i.picsum.photos/id/500/200/200.jpg",
    },
  },
];

const HomeScreen = ({ navigation, item }) => {
  const [tempArray, setTempArray] = useState(listArray);

  navigation.setOptions({
    headerLeft: () => likeListIcon(navigation),
    headerRight: () => userHomeListIcon(navigation),
  });

  React.useEffect(() => {
    crashlytics().log("App mounted.");
    SplashScreen.hide();
  }, []);

  const _onItemType = (item) => {
    if (item.type === "folder") {
      navigation.navigate("book", {
        name: item.subData,
        headerValue: item.value,
      });
    } else if (item.type === "book") {
      navigation.navigate("chapter", {
        bookData: item.uri,
        headerValue: item.value,
      });
    } else if (item.type === "audio") {
      navigation.navigate("music", {
        musicData: item.musicAdd,
        itemId: item.id,
      });
    } else if (item.type === "image") {
      //setImageData(item.imageUri);
      navigation.navigate("image", {
        imageData: item.imageUri,
        headerValue: item.value,
      });
      // AddRemoveImage(item.imageUri);
    } else {
      alert("No Data Found");
    }
  };

  const ItemView = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => _onItemType(item)}>
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              justifyContent: "center",
              marginLeft: wp("4%"),
              alignItems: "center",
              width: wp("10%"),
            }}
          >
            <Image
              source={
                item.type === "audio"
                  ? require("../assets/images/ic_music.png")
                  : null || item.type === "folder"
                  ? require("../assets/images/ic_folder.png")
                  : null || item.type === "book"
                  ? require("../assets/images/ic_file-text.png")
                  : null || item.type === "image"
                  ? require("../assets/images/ic_image.png")
                  : null
              }
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              marginLeft: wp("4%"),
              height: hp("7%"),
              width: wp("70%"),
            }}
          >
            <Text style={styles.item}>{item.value}</Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              width: wp("10%"),
              alignItems: "center",
            }}
          >
            <Image
              source={
                item.type === "audio"
                  ? require("../assets/images/ic_play-circle.png")
                  : null || item.type === "folder"
                  ? require("../assets/images/ic_chevron-right.png")
                  : null || item.type === "book"
                  ? null
                  : null || item.type === "image"
                  ? null
                  : null
              }
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const ItemSeparatorView = () => {
    return <View style={{ height: 0.5, backgroundColor: "#9B9A9B" }} />;
  };
  const chooseFile = () => {
    var options = {
      title: "Select Image",

      storageOptions: {
        skipBackup: true,
        path: "images",
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        let source = response;
        let dummyArray = [
          {
            // id: `${Math.floor(Math.random() * 100) + 1}`,
            value: Math.random().toString(36).substr(2, 5),
            type: "image",
            imageUri: response.uri,
          },
        ];
        dummyArray = [...dummyArray, ...tempArray];
        setTempArray(dummyArray);
      }
    });
  };
  const selectOneFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      console.log("res : " + JSON.stringify(res));
      console.log("URI : " + res.uri);
      console.log("Type : " + res.type);
      console.log("File Name : " + res.name);
      console.log("File Size : " + res.size);

      let dummyArray = [
        {
          value: Math.random().toString(36).substr(2, 5),
          type: "book",
          uri: res.uri,
        },
      ];
      dummyArray = [...dummyArray, ...tempArray];
      setTempArray(dummyArray);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert("Canceled from single doc picker");
      } else {
        //For Unknown Error
        alert("Unknown Error: " + JSON.stringify(err));

        throw err;
      }
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <ActionButton style={{ zIndex: 1 }} buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item
            buttonColor="#fff"
            title="Images "
            onPress={() => chooseFile()}
          >
            <Image
              source={require("../assets/images/ic_image.png")}
              style={{ tintColor: "black" }}
            />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor="#fff"
            title="Documents"
            onPress={selectOneFile}
          >
            <Image
              source={require("../assets/images/ic_file-text.png")}
              style={{ tintColor: "black" }}
            />
          </ActionButton.Item>

          <ActionButton.Item
            buttonColor="#fff"
            title="Get Info"
            // onPress={() => navigation.navigate("contactScreen")}
            onPress={() => navigation.navigate("apiScreen")}
          >
            <Image
              source={require("../assets/images/ic_contact-book.png")}
              style={{ tintColor: "black", height: 30, width: 30 }}
            />
          </ActionButton.Item>
        </ActionButton>
        <FlatList
          data={tempArray}
          ItemSeparatorComponent={ItemSeparatorView}
          renderItem={ItemView}
        />
      </View>

      {/* {imageData ? (
        <ImageModel
          imageData={imageData}
          closeImage={CloseImage}
          onPress={console.log("hey")}
        />
      ) : null} */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  item: {
    fontSize: wp("3.5%"),
    fontFamily: "Raleway-SemiBold",
    color: "#9B9A9B",
  },
  TouchableOpacityStyle: {
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    right: 0,
    bottom: 30,
    zIndex: 1,
  },

  FloatingButtonStyle: {
    position: "absolute",
    width: wp("100%"),
    height: 70,
    right: 0,
    bottom: 0,
    zIndex: 1,
  },
});

export default HomeScreen;
