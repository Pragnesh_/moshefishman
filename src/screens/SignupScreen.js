import React, { Component, useState, useEffect } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import DatePicker from "react-native-datepicker";
import CheckBox from "@react-native-community/checkbox";
import { validateEmail } from "../global/function";
import AsyncStorage from "@react-native-community/async-storage";
import store from "../../redux/stores/store";

const SignupScreen = ({ navigation }) => {
  const [date, setDate] = useState("");

  const [gender, setGender] = useState("male");
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [firstNameError, setFirstNameError] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastNameError, setLastNameError] = useState(false);
  const [lastName, setLastName] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [email, setEmail] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState("");
  const [datePickerRef, setDatePickerRef] = useState("");
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var da = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year

    setCurrentDate(da + "-" + month + "-" + year);
  }, []);
  const _onChangefirstName = (text) => {
    setFirstName(text);
    if (text === "") {
      setFirstNameError(true);
    } else {
      setFirstNameError(false);
    }
  };
  const _onChangeLname = (text) => {
    setLastName(text);
    if (text === "") {
      setLastNameError(true);
    } else {
      setLastNameError(false);
    }
  };
  const _onChangeEmail = (text) => {
    setEmail(text);
    if (validateEmail(text)) {
      setEmailError(false);
    } else {
      setEmailError(true);
    }
  };
  const _onChangePass = (text) => {
    setPassword(text);
    if (text === "") {
      setPasswordError(true);
    } else {
      setPasswordError(false);
    }
  };
  const _onChangeCpass = (text) => {
    setConfirmPassword(text);
    if (text === "") {
      setConfirmPasswordError(true);
    } else {
      setConfirmPasswordError(false);
    }
  };

  const _onRegister = () => {
    let userData = {
      userFname: firstName,
      userLname: lastName,
      userEmail: email,
      userDate: date,
    };

    if (firstName && lastName && password && confirmPassword) {
      if (date) {
        if (password === confirmPassword) {
          
          if (email && !emailError) {
            AsyncStorage.setItem("userData", JSON.stringify(userData));
            store.dispatch({ type: "USER_DATA", payload: userData });
            navigation.navigate("home");
          } else {
            alert(
              "You have entered an invalid e-mail address. Please try again"
            );
          }
        } else {
          alert("Password must be Same");
        }
      } else {
        alert("Please select Date of Birth");
      }
    } else {
      alert("Please fill up above information");
    }
  };

  return (
    <View style={styles.MainContainer}>
      <DatePicker
        style={{
          position: "absolute",
        }}
        date={date}
        mode="date"
        showIcon={false}
        hideText={true}
        format="DD-MM-YYYY"
        maxDate={currentDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        ref={(dateReference) => {
          setDatePickerRef(dateReference);
        }}
        onDateChange={(date) => {
          setDate(date);
        }}
      />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ alignItems: "center" }}
      >
        <View style={{ marginTop: hp("4%"), width: wp("90%") }}>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_user_name.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="First Name"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangefirstName(text);
              }}
            />
            {firstNameError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>

          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_user_name.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="Last Name"
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangeLname(text);
              }}
            />
            {lastNameError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>
          <View style={styles.SectionStyle}>
            <Image
              source={require("../assets/images/ic_email_black.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text, email) => {
                _onChangeEmail(text);
              }}
            />
            {emailError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={{ height: 15, width: 15, marginRight: 10 }}
              />
            ) : null}
          </View>
          <View style={styles.SectionStyle}>
            <TouchableOpacity
              style={{ flexDirection: "row" }}
              onPress={() => {
                datePickerRef.onPressDate();
              }}
            >
              <Image
                source={require("../assets/images/ic_dob.png")}
                style={[styles.ImageStyle, { alignSelf: "center" }]}
              />
              {date ? (
                <Text style={styles.TextInputStyleClass}>{date}</Text>
              ) : (
                <Text style={styles.TextInputStyle}>Date Of Birth</Text>
              )}
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Image
              source={require("../assets/images/ic_sex.png")}
              style={styles.ImageStyle}
            />
            <Text
              style={{
                marginLeft: wp("3%"),
                fontSize: wp("4.2%"),
                fontFamily: "Raleway-Regular",
              }}
            >
              Gender
            </Text>
          </View>
          <View style={styles.RadioMainView}>
            <TouchableOpacity
              onPress={() => {
                setGender("male");
              }}
              style={styles.RadioTouchable}
            >
              <Image
                source={
                  gender === "male"
                    ? require("../assets/images/ic_radio_button_checked.png")
                    : require("../assets/images/ic_radio_button_unchecked.png")
                }
              />
              {gender === "male" ? (
                <Text style={styles.RadioTextSelect}>Male</Text>
              ) : (
                <Text style={styles.RadioTextUnselect}>Male</Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setGender("female");
              }}
              style={styles.RadioTouchable}
            >
              <Image
                source={
                  gender === "female"
                    ? require("../assets/images/ic_radio_button_checked.png")
                    : require("../assets/images/ic_radio_button_unchecked.png")
                }
              />
              {gender === "female" ? (
                <Text style={styles.RadioTextSelect}>Female</Text>
              ) : (
                <Text style={styles.RadioTextUnselect}>Female</Text>
              )}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                setGender("divers");
              }}
              style={styles.RadioTouchable}
            >
              <Image
                source={
                  gender === "divers"
                    ? require("../assets/images/ic_radio_button_checked.png")
                    : require("../assets/images/ic_radio_button_unchecked.png")
                }
              />
              {gender === "divers" ? (
                <Text style={styles.RadioTextSelect}>Divers</Text>
              ) : (
                <Text style={styles.RadioTextUnselect}>Divers</Text>
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.PasswordSectionStyle}>
            <Image
              source={require("../assets/images/ic_user_password.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              secureTextEntry={true}
              placeholder="Password"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangePass(text);
              }}
            />
            {passwordError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={styles.PasswordErrorStyle}
              />
            ) : null}
          </View>
          <View style={[styles.SectionStyle, { marginBottom: 5 }]}>
            <Image
              source={require("../assets/images/ic_user_password.png")}
              style={styles.ImageStyle}
            />
            <TextInput
              style={styles.TextInputStyleClass}
              secureTextEntry={true}
              placeholder="Confirm Password"
              underlineColorAndroid="transparent"
              placeholderTextColor="#9B9A9B"
              onChangeText={(text) => {
                _onChangeCpass(text);
              }}
            />
            {confirmPasswordError ? (
              <Image
                source={require("../assets/images/error.png")}
                style={styles.PasswordErrorStyle}
              />
            ) : null}
          </View>
          <View style={styles.CheckboxMainView}>
            {toggleCheckBox ? (
              <TouchableOpacity
                onPress={() => {
                  setToggleCheckBox(false);
                }}
              >
                <Image source={require("../assets/images/ic_unchecked.png")} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  setToggleCheckBox(true);
                }}
              >
                <Image source={require("../assets/images/ic_checked.png")} />
              </TouchableOpacity>
            )}
            <Text style={styles.CheckboxTextSelect}>Remember Me</Text>
          </View>

          <View style={{ paddingVertical: hp("4%"), alignItems: "center" }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => {
                _onRegister();
              }}
            >
              <Text style={styles.loginText}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>

        <SafeAreaView />
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
  },
  loginScreenButton: {
    width: wp("85%"),
    backgroundColor: "#030504",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  loginText: {
    color: "#FFFFFF",
    padding: 10,
    fontSize: wp("5%"),
    fontFamily: "Raleway-SemiBold",
    paddingVertical: hp("1.75%"),
  },

  ImageStyle: {
    marginLeft: wp("3%"),
    alignItems: "center",
  },

  TextInputStyleClass: {
    flex: 1,
    fontSize: 17,
    color: "#030504",
    marginLeft: wp("3%"),
    fontFamily: "Raleway-Regular",
    paddingVertical: hp("1%"),
  },
  TextInputStyle: {
    flex: 1,
    fontSize: 17,
    color: "#9B9A9B",
    marginLeft: wp("3%"),
    fontFamily: "Raleway-Regular",
    paddingVertical: hp("1.3%"),
  },

  RadioMainView: {
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    justifyContent: "space-around",
    marginTop: 10,
  },
  CheckboxMainView: {
    flexDirection: "row",
    paddingHorizontal: wp("2.75%"),
    height: 30,
    alignItems: "center",
  },
  CheckboxTextSelect: {
    fontSize: wp("4%"),
    fontFamily: "Raleway-Regular",
    color: "#030504",
    marginHorizontal: 7,
  },
  RadioTouchable: { flexDirection: "row", alignItems: "center" },
  RadioTextSelect: {
    marginLeft: 4,
    fontSize: wp("4.2%"),
    fontFamily: "Raleway-Regular",
    color: "#030504",
  },
  RadioTextUnselect: {
    marginLeft: 4,
    fontSize: wp("4.2%"),
    fontFamily: "Raleway-Regular",
    color: "#A5A5A5",
  },
  SectionStyle: {
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9B9A9B",
    paddingVertical: hp("0.6%"),
  },
  PasswordSectionStyle: {
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9B9A9B",
    marginTop: 20,
    paddingVertical: hp("0.8%"),
  },
  PasswordErrorStyle: { height: 15, width: 15, marginRight: 10 },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
});

export default SignupScreen;
