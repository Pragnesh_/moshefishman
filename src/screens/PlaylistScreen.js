import React, { useEffect } from "react";
import { StyleSheet, Text, View,TouchableOpacity ,Image} from "react-native";
import TrackPlayer, { usePlaybackState } from "react-native-track-player";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Player from "../components/Player";
import playlistData from "../../data/playlist.json";
import localTrack from "../../resources/pure.m4a";
import userHomeIcon from "../global/userHomeIcon";

export default function PlaylistScreen({ route, navigation }) {
  navigation.setOptions({
    headerTitle: route.params.headerValue,
    headerRight: () => userHomeIcon(navigation),
  });
  
  const playbackState = usePlaybackState();

  useEffect(() => {
    setup();
  }, []);

  async function setup() {
    await TrackPlayer.setupPlayer({});
    await TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
      ],
    });
  }

  async function togglePlayback() {
    const currentTrack = await TrackPlayer.getCurrentTrack();

    if (currentTrack == null) {
      await TrackPlayer.reset();

      await TrackPlayer.add(route.params.musicData);
      // await TrackPlayer.add({
      //   id: "local-track",
      //   url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
      //   title: "Music 1",
      //   artist: "David Chavez",
      //   artwork: "https://i.picsum.photos/id/500/200/200.jpg",
      //   duration: 28,
      // });
      await TrackPlayer.play();
    } else {
      
      console.log("playbackState", playbackState);
      console.log("TrackPlayer.STATE_PAUSED", TrackPlayer.STATE_PAUSED);
      if (playbackState === TrackPlayer.STATE_PAUSED) {
        await TrackPlayer.play();
      } else {
        await TrackPlayer.pause();
      }
    }
  }

  async function toggleReset() {
    await TrackPlayer.reset();
  }

  return (
    <View style={styles.container}>

      <Player onTogglePlayback={togglePlayback} onStop={toggleReset} onRoute={route} />
      {/* <Text style={styles.state}>{getStateName(playbackState)}</Text> */}
    </View>
  );
}

// PlaylistScreen.navigationOptions = {
//   title: "Playlist Example",
// };

function getStateName(state) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return "None";
    case TrackPlayer.STATE_PLAYING:
      return "Playing";
    case TrackPlayer.STATE_PAUSED:
      return "Paused";
    case TrackPlayer.STATE_STOPPED:
      return "Stopped";
    case TrackPlayer.STATE_BUFFERING:
      return "Buffering";
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
  },
  description: {
    width: "80%",
    marginTop: 20,
    textAlign: "center",
  },

  state: {
    marginTop: 10,
  },
});
