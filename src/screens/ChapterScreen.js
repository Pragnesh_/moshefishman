import React, { Component, useState } from "react";
import {
  StyleSheet, Dimensions, View,SafeAreaView
} from "react-native";
import Pdf from 'react-native-pdf';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import userHomeIcon from "../global/userHomeIcon";

const ChapterScreen = ({ route, navigation }) => {
  const source = {uri:route.params.bookData,cache:true};
  navigation.setOptions({
    headerTitle: route.params.headerValue,
    headerRight: () => userHomeIcon(navigation),
  });
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
      <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    onPressLink={(uri)=>{
                        console.log(`Link presse: ${uri}`)
                    }}
                    style={styles.pdf}/>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    fontSize: wp("3.5%"),
    fontFamily: "Raleway-SemiBold",
    color: "#9B9A9B",
  },
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
}
});

export default ChapterScreen;
