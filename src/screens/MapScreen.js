import React, { useEffect, useState } from "react";
import { Image, Text, ActivityIndicator, StyleSheet, View } from "react-native";
import RNLocation from "react-native-location";

import MapView, {
  Callout,
  Marker,
  AnimatedRegion,
  PROVIDER_GOOGLE,
} from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";

const initialState = {
  latitude: null,
  longitude: null,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};
const MapScreen = () => {
  useEffect(() => {
    Geolocation.watchPosition(
      (position) => {
        console.log("location Position->>", JSON.stringify(position));
        const { longitude, latitude } = position.coords;
        console.log("latitude->>", latitude);
        console.log("Longitude-->", longitude);
        setCurrentPosition({
          ...currentPosition,
          latitude,
          longitude,
        });
      },
      (error) => alert(error.message),
      { timeout: 20000, maximumAge: 1000 }
    );
  }, []);
  const [currentPosition, setCurrentPosition] = useState(initialState);

  return currentPosition.latitude ? (
    <MapView
      style={{ flex: 1, loadingEnabled: true }}
      provider={PROVIDER_GOOGLE}
      showsUserLocation
      loadingEnabled={true}
      loadingIndicatorColor="red"
      loadingBackgroundColor="yellow"
      initialRegion={currentPosition}
    >
      <Marker coordinate={currentPosition}>
        <Callout>
          <Text>IBL infotech</Text>
          <Image source={require("../assets/images/ic_businessman.png")} />
        </Callout>
      </Marker>
    </MapView>
  ) : (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#000" />
    </View> 
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
});

export default MapScreen;
