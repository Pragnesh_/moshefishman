// import React, { useState } from "react";

// import {
//   SafeAreaView,
//   View,
//   Button,
//   StyleSheet,
//   Text,
//   FlatList,
//   ActivityIndicator,
// } from "react-native";
// import { GiftedChat } from "react-native-gifted-chat";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from "react-native-responsive-screen";
// import messages from "../components/message";

// const ChatScreen = () => {
//   const [message, setMessage] = useState(messages);
//   console.log("message", messages);

//   const onSend = (msg = []) => {
//     setMessage(GiftedChat.append(message, msg));
//   };

//   return (
//     <GiftedChat
//       messages={message}
//       onSend={(message) => onSend(message)}
//       user={{
//         _id: 1,
//       }}
//     />
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//   },
// });

// export default ChatScreen;
import React, { Component, useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  TextInput,
  FlatList,
  Button,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import moment from "moment";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const ChatScreen = () => {
  const [height, setHeight] = useState(0);
  const [msg, setMsg] = useState("");
  const [message, setMessage] = useState([
    {
      id: 1,
      sent: true,
      msg: "Lorem ipsum dolor",
      image: "https://www.bootdey.com/img/Content/avatar/avatar1.png",
      createdAt: "12:44",
    },
    {
      id: 2,
      sent: true,
      msg: "sit amet, consectetuer",
      image: "https://www.bootdey.com/img/Content/avatar/avatar1.png",
      createdAt: "12:44 pm",
    },
    {
      id: 3,
      sent: false,
      msg: "adipiscing elit. Aenean ",
      image: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
      createdAt: "12:44 pm",
    },
    {
      id: 4,
      sent: true,
      msg: "commodo ligula eget dolor.",
      image: "https://www.bootdey.com/img/Content/avatar/avatar1.png",
      createdAt: "12:46 pm ",
    },
    {
      id: 5,
      sent: false,
      msg:
        "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes",
      image: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
      createdAt: "12:48 pm",
    },
    {
      id: 6,
      sent: true,
      msg:
        "nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo",
      image: "https://www.bootdey.com/img/Content/avatar/avatar1.png",
      createdAt: "12:50 pm",
    },
    {
      id: 7,
      sent: false,
      msg: "rhoncus ut, imperdiet",
      image: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
      createdAt: "12:52 pm",
    },
    {
      id: 8,
      sent: false,
      msg: "a, venenatis vitae",
      image: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
      createdAt: "12:59 pm",
    },
  ]);
  const reply = () => {
    var date = moment().utcOffset("+05:30").format(" hh:mm a");
    var messages = message;
    messages.push({
      id: Math.floor(Math.random() * 99 + 1),
      sent: false,
      msg: msg.trim(),
      image: "https://www.bootdey.com/img/Content/avatar/avatar6.png",
      createdAt: date,
    });
    setMsg("");
    setMessage(messages);
  };

  const send = () => {
    if (msg.trim().length > 0) {
      var messages = message;

      var date = moment().utcOffset("+05:30").format(" hh:mm a");

      messages.push({
        id: Math.floor(Math.random() * 99 + 1),
        sent: true,
        msg: msg.trim(),
        image: "https://www.bootdey.com/img/Content/avatar/avatar1.png",
        createdAt: date,
      });
      setMessage(messages);
      setTimeout(() => {
        reply();
      }, 2000);
    }
  };
  const _chatItem = ({ item }) => {
    if (item.sent === false) {
      return (
        <View style={styles.eachMsg}>
          <Image source={{ uri: item.image }} style={styles.userPic} />
          <View style={styles.msgBlock}>
            <Text style={styles.msgTxt}>{item.msg}</Text>
            <Text style={styles.leftTxt}>{item.createdAt}</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.rightMsg}>
          <View style={styles.rightBlock}>
            <Text style={styles.rightTxt}>{item.msg}</Text>
            <Text style={styles.rightMsgTxt}>{item.createdAt}</Text>
          </View>
          <Image source={{ uri: item.image }} style={styles.userPic} />
        </View>
      );
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: "black" }}>
      <FlatList
        data={message}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={_chatItem}
        ref={(ref) => {
          myFlatListRef = ref;
        }}
        onContentSizeChange={() => {
          myFlatListRef.scrollToEnd({ animated: true });
        }}
        onLayout={() => {
          myFlatListRef.scrollToEnd({ animated: true });
        }}
      />

      <View
        style={{
          borderColor: "#778899",
          borderWidth: 2,
          margin: wp("3%"),
          borderRadius: 20,
          flexDirection: "row",
        }}
      >
        <TextInput
          value={msg}
          style={styles.txtInput}
          placeholderTextColor="silver"
          onChangeText={(msg) => setMsg(msg)}
          placeholder="Type a message"
          multiline={true}
          onContentSizeChange={(e) =>
            setHeight(e.nativeEvent.contentSize.height)
          }
        />

        <TouchableOpacity style={styles.btnSend} onPress={() => send()}>
          <Image
            source={{
              uri: "https://img.icons8.com/small/75/ffffff/filled-sent.png",
            }}
            style={styles.iconSend}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  txtInput: {
    width: wp("88%"),
    marginLeft: 10,
    fontSize: hp("2.2%"),
    borderRadius: 5,
    color: "#FFF",
    maxHeight: 100,
  },
  btnSend: {
    width: 32,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginLeft: -25,
    backgroundColor: "#778899",
    borderRadius: 360,
  },
  iconSend: {
    width: 20,
    height: 20,
    alignSelf: "center",
    tintColor: "#FFF",
    marginLeft: 2,
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
  },

  right: {
    flexDirection: "row",
  },
  chatTitle: {
    color: "#fff",
    fontWeight: "600",
    margin: 10,
    fontSize: 15,
  },
  chatImage: {
    width: 30,
    height: 30,
    borderRadius: 15,
    margin: 5,
  },
  eachMsg: {
    flexDirection: "row",
    alignItems: "flex-end",
    margin: 5,
  },
  rightMsg: {
    flexDirection: "row",
    alignItems: "flex-end",
    margin: 5,
    alignSelf: "flex-end",
  },
  userPic: {
    height: 40,
    width: 40,
    margin: 5,
    borderRadius: 20,
  },
  msgBlock: {
    width: 220,
    borderRadius: 5,
    backgroundColor: "#C0C0C0",
    padding: 10,
  },
  rightBlock: {
    width: 220,
    borderRadius: 5,
    backgroundColor: "#2F4F4F",
    padding: 10,
  },
  msgTxt: {
    fontSize: 15,
    color: "#000",
  },
  rightTxt: {
    fontSize: 15,
    color: "#FFF",
  },
  leftTxt: {
    fontSize: 15,
    color: "#000",
    alignSelf: "flex-end",
  },
  rightMsgTxt: {
    fontSize: 15,
    color: "#FFF",
    alignSelf: "flex-end",
  },
});

export default ChatScreen;
