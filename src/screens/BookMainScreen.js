import React, { Component, useState } from "react";
import {
  Button,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  FlatList,
} from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import TrackPlayer from "react-native-track-player";
import ImageModel from "../components/ImageModel";
import userHomeIcon from "../global/userHomeIcon";

const BookMainScreen = ({ route, navigation }) => {
  const [imageData, setImageData] = useState("");

  navigation.setOptions({
    headerTitle: route.params.headerValue,
    headerRight: () => userHomeIcon(navigation),
  });

  const _onItemType = (item, index) => {
    if (item.type === "folder") {
      navigation.push("book", {
        name: item.subChapterData,
        headerValue: item.value,
      });
    } else if (item.type === "book") {
      navigation.navigate("chapter", {
        bookData: item.uri,
        headerValue: item.value,
      });
    } else if (item.type === "audio") {
      navigation.navigate("music", {
        musicData: item.musicAdd,
        headerValue: item.value,
      });
      TrackPlayer.reset();
    } else if (item.type === "image") {
      //setImageData(item.imageUri);
      navigation.navigate("image", {
        imageData: item.imageUri,
        headerValue: item.value,
      });
    } else {
      alert("No Data Found");
    }
  };

  const ItemView = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => _onItemType(item, index)}>
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              justifyContent: "center",
              marginLeft: wp("4%"),
              alignItems: "center",
              width: wp("10%"),
            }}
          >
            <Image
              source={
                item.type === "audio"
                  ? require("../assets/images/ic_music.png")
                  : null || item.type === "folder"
                  ? require("../assets/images/ic_folder.png")
                  : null || item.type === "book"
                  ? require("../assets/images/ic_file-text.png")
                  : null || item.type === "image"
                  ? require("../assets/images/ic_image.png")
                  : null
              }
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              marginLeft: wp("4%"),
              height: hp("7%"),
              width: wp("70%"),
            }}
          >
            <Text style={styles.item}>{item.value}</Text>
          </View>
          <View
            style={{
              justifyContent: "center",

              width: wp("10%"),
              alignItems: "center",
            }}
          >
            <Image
              source={
                item.type === "audio"
                  ? require("../assets/images/ic_play-circle.png")
                  : null || item.type === "folder"
                  ? require("../assets/images/ic_chevron-right.png")
                  : null || item.type === "book"
                  ? null
                  : null || item.type === "image"
                  ? null
                  : null
              }
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const ItemSeparatorView = () => {
    return <View style={{ height: 0.5, backgroundColor: "#9B9A9B" }} />;
  };

  const getItem = (item) => {
    alert("Id : " + item.id + " Value : " + item.value);
  };

  // const CloseImage = () => {
  //   setImageData();
  // };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <FlatList
          data={route.params.name}
          ItemSeparatorComponent={ItemSeparatorView}
          renderItem={ItemView}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
      {/* {imageData ? (
        <ImageModel imageData={imageData} closeImage={CloseImage} />
      ) : null} */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    fontSize: wp("3.5%"),
    fontFamily: "Raleway-SemiBold",
    color: "#9B9A9B",
  },
});

export default BookMainScreen;
