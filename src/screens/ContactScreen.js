/* This is an Access Contact List example from https://aboutreact.com/ */
/* https://aboutreact.com/access-contact-list-react-native/ */

//Import React
import React, { useState, useEffect } from "react";

//Import all required component
import {
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
} from "react-native";

import Contacts from "react-native-contacts";

const ContactScreen = () => {
  useEffect(() => {
    Contacts.checkPermission((err, permission) => {
      // Contacts.PERMISSION_AUTHORIZED || Contacts.PERMISSION_UNDEFINED || Contacts.PERMISSION_DENIED
      if (permission === "undefined") {
        Contacts.requestPermission((err, permission) => {
          if (permission === "authorized") {
            _onOpenContactList();
          }
        });
        
      }
      if (permission === "authorized") {
        _onOpenContactList();
      }
      if (permission === "denied") {
        if (Platform.OS === "android") {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
            {
              title: strings("contactPermission.title"),
              message: strings("contactPermission.message"),
              buttonPositive: strings("contactPermission.buttonPositive"),
            }
          )
            .then((response) => {
              console.log("response=>>", response);
              if (response === "denied") {
              } else if (response === "never_ask_again") {
                //this._onContactClickOpenSetting();
              } else {
                _onOpenContactList();
              }
            })
            .catch((error) => {
              console.log("ContactError=>>", error);
            });
        } else {
          //this._onContactClickOpenSetting();
        }
      }
    });
  }, []);

  const _onOpenContactList = () => {
    Contacts.getAll((err, res) => {
      if (err === "denied") {
        // error
        return;
        // alert('please allow contacts permission from Device Setting')
      } else {
        // contacts returned in Array
        console.log("response",res);
      }
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Text style={styles.header}>Access Contact List in React Native</Text>
        {/* <TextInput
          onChangeText={search}
          placeholder='Search'
          style={styles.searchBar}
        />
        <FlatList
          data={contacts}
          renderItem={(contact) => {
            {console.log('contact -> ' + JSON.stringify(contact))}
            return (<ListItem
              key={contact.item.recordID}
              item={contact.item}
              onPress={openContact}
            />)
          }}
          keyExtractor={item => item.recordID}
        /> */}
      </View>
    </SafeAreaView>
  );
};
export default ContactScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: "#4591ed",
    color: "white",
    paddingHorizontal: 15,
    paddingVertical: 15,
    fontSize: 20,
  },
  searchBar: {
    backgroundColor: "#f0eded",
    paddingHorizontal: 30,
    paddingVertical: Platform.OS === "android" ? undefined : 15,
  },
});
