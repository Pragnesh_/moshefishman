import React from "react";
import { StyleSheet, View, SafeAreaView, Image } from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Modal from "react-native-modal";
import ImageLoad from "react-native-image-placeholder";

const ImageModel = (props) => {
  return (
    <SafeAreaView>
      <Modal
        isVisible={true}
        onBackdropPress={() => props.closeImage()}
        onSwipeComplete={() => props.closeImage()}
        swipeDirection={["left", "down", "right", "up"]}
      >
        <View style={{ height: hp("99%") }}>
          <ImageLoad
            isShowActivity={true}
            style={{ width: "100%", height: "100%" }}
            resizeMode="contain"
            
            source={{
              uri: props.imageData,
            }}
          />
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default ImageModel;
