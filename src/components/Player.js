import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import TrackPlayer, {
  useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents,
  TrackPlayerEvents,
} from "react-native-track-player";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes,
} from "react-native";

function ProgressBar({ onTrue }) {
  const progress = useTrackPlayerProgress();

  let input = progress.position;
  input = input % (24 * 3600);
  

  input %= 3600;
  let minutes = input / 60;
  let minute = parseInt(minutes);
  var minuteNumber = ("0" + minute).slice(-2);
  input %= 60;
  let seconds = input;

  let second = parseInt(seconds);
  var secondNumber = ("0" + second).slice(-2);

  return (
    <View style={styles.progress}>
      <View style={{ flex: progress.position, backgroundColor: "black" }} />

      <View
        style={{
          flex: progress.duration - progress.position,
          backgroundColor: "grey",
        }}
      >
        <Text
          style={{
            position: "absolute",
            top: 5,
            left: -20,
            fontSize: 15,
            width: 50,
          }}
        >
          {minuteNumber + ":" + secondNumber}
        </Text>
      </View>
    </View>
  );
}

// function Timer(position) {
//   let input = position;
//   input = input % (24 * 3600);
//   let hour = input / 3600;

//   input %= 3600;
//   let minutes = input / 60;

//   input %= 60;
//   let seconds = input;

//   console.log("hour --> ", parseInt(hour));
//   console.log("minutes --> ", parseInt(minutes));
//   console.log("seconds --> ", parseInt(seconds));
// }

function ControlButton({ title, onPress }) {
  return (
    <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
      <Image
        style={{
          height: 40,
          width: 40,
          justifyContent: "center",
          alignSelf: "center",
        }}
        source={
          title
            ? require("../assets/images/ic_play.png")
            : require("../assets/images/ic_pause.png")
        }
      />
    </TouchableOpacity>
  );
}

ControlButton.propTypes = {
  title: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default function Player(props) {
  const events = [
    TrackPlayerEvents.PLAYBACK_STATE,
    TrackPlayerEvents.PLAYBACK_ERROR,
  ];

  const playbackState = usePlaybackState();
  const [trackTitle, setTrackTitle] = useState(
    props.onRoute.params.musicData.title
  );
  const { position, bufferedPosition, duration } = useTrackPlayerProgress();

  useTrackPlayerEvents(events, (event) => {
    if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
      setTrackTitle(title);
    }
    if (
      position.toFixed(0) === duration.toFixed(0) &&
      event.state === "paused"
    ) {
      onStop();
    }
  });

  const { style, onTogglePlayback, onStop, onRoute } = props;

  var middleButtonText = true;

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    middleButtonText = false;
  }

  return (
    <View>
      <Image
        style={styles.cover}
        source={require("../assets/images/img_music.png")}
      />
      <Text style={styles.title}>{trackTitle}</Text>
      <ProgressBar />
      <View style={styles.controls}>
        <ControlButton title={middleButtonText} onPress={onTogglePlayback} />
      </View>
    </View>
  );
}

Player.propTypes = {
  style: ViewPropTypes.style,
  onTogglePlayback: PropTypes.func.isRequired,
  onStop: PropTypes.func.isRequired,
  onRoute: PropTypes.object,
};

Player.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  cover: {
    width: 120,
    height: 120,
    marginTop: 70,
    justifyContent: "center",
    alignSelf: "center",
  },
  progress: {
    height: 5,
    width: "70%",
    marginTop: 20,
    flexDirection: "row",
  },
  title: {
    alignSelf: "center",
    marginTop: 20,
    fontWeight: "bold",
  },

  controls: {
    marginVertical: 20,
    marginTop: 30,
    flexDirection: "row",
  },
  controlButtonContainer: {
    flex: 1,
  },
  controlButtonText: {
    fontSize: 18,
    textAlign: "center",
  },
});
