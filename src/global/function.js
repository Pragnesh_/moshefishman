import { getMethod } from "./services";

export const validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let Bool = re.test(email);
  return new Promise((resolve, reject) => {
    if (Bool) {
      resolve(true);
    } else {
      reject(false);
    }
  });
};

export const resolveResponse = (data) => {
  return new Promise((resolve) => {
    let tmpData = data.json();
    resolve(tmpData);
  });
};

export const getUniversity = () => {
  return new Promise((resolve, reject) => {
    getMethod("/appuser/university")
      .then((res) => resolve(res))
      .catch((error) => {
        reject(error);
      });
  });
};
