import { url } from "./constants";
import { resolveResponse } from "./function";

export const getMethod = (urlEndPoint) => {
  return new Promise((resolve, reject) => {
    fetch(url + urlEndPoint, {
      method: "GET",
    })
      .then((response) => {
        if (response.status === 204) {
          reject({
            code: response.status,
            message: "No data found",
          });
        } else {
          resolveResponse(response).then((responseJson) => {
            if (response.status === 200) {
              console.log("responseJson->>", responseJson);

              resolve(responseJson);
            } else {
              reject({
                code: response.status,
                message: responseJson.response_data.error,
              });
            }
          });
        }
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};
