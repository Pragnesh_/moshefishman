import React, { Component, useState } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const userHomeListIcon = (navigation) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity
        style={{ justifyContent: "center", marginRight: wp("5%") }}
        onPress={() => navigation.navigate("chatScreen")}
      >
        <Image
          style={{ tintColor: "#FFF", marginTop: 5 }}
          source={require("../assets/images/ic_facebook-chat-balloons.png")}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={{ justifyContent: "center", marginRight: wp("5%") }}
        onPress={() => navigation.navigate("profile")}
      >
        <Image source={require("../assets/images/ic_user.png")} />
      </TouchableOpacity>
    </View>
  );
};

export default userHomeListIcon;
