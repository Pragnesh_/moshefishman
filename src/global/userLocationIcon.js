import React, { Component, useState } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const userLocationIcon = (navigation) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity
        style={{ justifyContent: "center", marginRight: wp("5%") }}
        onPress={() => navigation.navigate("mapScreen")}
      >
        <Image style={{tintColor:"#FFF"}} source={require("../assets/images/ic_pointer.png")} />
      </TouchableOpacity>
    </View>
  );
};

export default userLocationIcon;
