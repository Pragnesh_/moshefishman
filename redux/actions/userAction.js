import {USER_DATA,LIKE_IMAGE} from "../actions/actionType"

export const user_data = (data) => {
    return async dispatch => {
      dispatch({
          type: USER_DATA,
          payload: data
      })
    }
  }

  export const like_image = (data) => {
    return async dispatch => {
      dispatch({
          type: LIKE_IMAGE,
          payload: data
      })
    }
  }