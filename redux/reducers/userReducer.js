import { LIKE_IMAGE, USER_DATA } from "../actions/actionType";

const initialState = {
  userData: null,
  likeImage: [],
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };
    case LIKE_IMAGE:
      let imageArray = initialState.likeImage;

      let uri = action.payload;
      if (imageArray.length) {
        let index = imageArray.findIndex((obj) => obj === uri);
        if (index !== -1) {
          imageArray.splice(index, 1);
        } else {
          imageArray.push(action.payload);
        }
      } else {
        imageArray.push(action.payload);
      }

      return {
        ...state,
        likeImage: imageArray,
      };
    default:
      return state;
  }
};
